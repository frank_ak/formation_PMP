## Élaborer l'échéancier

### 6.5 élaboration de l'écheancier

On reprend les attributs(les dates, les personnes, le risque associé) de nos activités.
C'est un processus qui est élaborer de façon itérative.

Les étapes prinicpales de développement : 
* définir les jalons
* identifier et séquencer les activités
* Estimer les durées

Cela permet de voir la charge au niveau des ressources et grâce à des techniques (Le nivellement et le report d'activité ) permettant de réduire la charge de ressources trop sollicitées.

cours 06 - slide 58 

Les techniques d'optimisations des ressources, Méthode du chemin critique, compression d'écheancier.

Diagramme en réseau : 
* date au plus tard de début
* date au plus tard de fin
* date de début au plus tôt (ES = early start)
* date de fin au plus tôt (EF)
* Marge totale
* Marge libre

Marge totale
> total float = LS - ES (d'une activité)

Marge libre
>Free float = ES(successeur) - EF(predecesseur)

chemin critique est l'ensemble des étapes critiques du diagramme.


**Le nivellement:(leveling)** : 
    * ajustement des dates de début et de fin sur les contraintes, il modifie souvent le chemin critique parce que la marge disponible est utilisée au nivellement

**Le lissage(smoothing)**

**Analyse des données**

* What-if analysis (cela permet de prendre des réserves ou des contingences de temps, et d'élaborer des solutions alternatives)

**Technique de compression de planning**

* compression des délais (crashing)
* Fast tracking ( Exécution accélérée par chevauchement)

**Planning de release agile**
* Le planning des releases agiles donne une vue à haut niveau de la ligne de temps(sommaire) des releases basée sur :
* 


### 7.3 Déterminer le budget

Réconciliation des limites de financement : l'opération utilisée pour

 Le budget du projet se compose de :
* estimations de coût des activités consolidées + contingence des activités =  estimation des Lots de Travail
    * estimation des lots de travail + Réserve de contingence = référence de base des coûts = Comptes de contrôle(cost baseline)
    * référence de base des coûts + Réserve de management = budget du projet

## Planifier les risques

### 11.2 Identifier les risques

L’identification des risques concerne :
* Les individuels du projet
* Les risques généraux du projet : il s'agit de ceux qui sont souvent identifier dans la charte et que l'on peut creuser dans les processus d'identification des risques.

L'identification c'est l'enregistrement de tous les risques dans le **registre de risques** et potentiellement des réponses associées et des responsables/ceux qui vont résoudre du risque.

On le fait souvent avec un SME(subject matter expert).

En entrée on peut tout mettre car potentiellemnet tout peut être un risque.

En particulier, on choisira les estimations, les parties prenantes.


### 11.3 Mettre en œuvre l'analyse qualitative des risques

L’analyse qualitative des risques permet
* De définir des priorités dans le traitement des risques individuels du projet 
* En évaluant leur probabilité d’occurrence et leur impact

 Le bénéfice majeur du processus est d’aider à se concentrer avant tout sur les risques de plus grande priorité


** Les outils et techniques**
Analyse de données
* Évaluation de la qualité des données sur les risques
* Evaluation de la probabilité et de l’ impact des risques
* Evaluation d’autres paramètres de risque

Si on utilise de mauvaises données , cela impliquera la plupart du temps une mauvaise gestion du risque ou une mauvaise prise en compte du risque.

On évaluera la **probabilité** et l'**impact** des risques. Bien souvent cela s'avère suffisant.

Les autres paramètres du risque: 
* L'urgence
* la proximité
* La dormance : combien de temps entre l'occurence du risque et ses effets.
* son caractère Gérable
* Sa contrôlabiilté
* la connectivité
* impact stratégique
* **propinquity** ( la perception du risque)

On catégorise tout cela à travers un RBS pour éviter d'oublier des risques et avoir un vue plus claire de l'ensemble des risques.

**Matrice PROBABILITÉ/IMPACT**


### 11.4 Mettre en œuvre l’analyse quantitative des risques

Encore une fois avoir des bonnes données est vital.

L'algorithme de monte carlo se base sur le hasard (**RANDOM**) : le mot clé est **RANDOMLY** pour toutes les questions liées à monte carlo.

l'analyse quantitative représente une mise à jour du rapport du risque.

Les différentes analyses : 

* le calcul de la valeur **monétaire attendue**
EMV : expected Monetary Value
* Analyse par abre de décision

> si on combine les deux on aura un **Net path value** (NPV)


### 11.5 Planifier les réponses aux risques
le but est de construire le plan de réponse au risque.
C'est le processus qui sonciste à developper des options et de s'accorder sur des actions permettant : 
* d'améliorer les opportunités
* de reduire les menaces relatives aux objectifs du projet
* plus généralement de surveiller le niveau d'exposition du projet

5 types de stratégie de réponses au risques pour 5 types de réponses possibles.

stratégie de réponse au risques:
* Escalader
* éviter
* Transférer
* Atténuer (Mitigate)
* Accepter ( acceptation passive et active)

stratégie de réponses au opportunités:
* Escalader
* exploiter
* Partager
* Améliorer (Enhance)
* Accepter



## Exécution

On a déjà signé notre baseline et notre plan de mangement projet. Nous pouvons passer à l'établissement des processus d'exécution.

les processus d'exécution sont assez important car il s'agit des aspects humains liés au projet la plupart du temps.

la planification étant terminée on peut faire un kick-off et passer à l'exécution.

Le but de ces porcessus est de veuiller à ce que l'exécution du projet soit fait de manière conforme, pour relever les indicateurs de performances.

Il compte 10 processus 

### 4.3 Diriger et piloter l’exécution du projet

C'est le processus qui consiste à exécuter (faire exécuter) tout le travail tel qu'il a été précisé dans le contenu du projet (project scope et formalisé dans le WBS

Le chef de projet veille dans ce processus à l’utilisation des ressources, à leur performance et à la production des livrables décrits dans le plan de management de projet

Ce processus demande également un suivi de l’impact de tous les changements sur le projet

### Gérer la connaissance du projet
En sortie on a le registre des connaissances qui permet à l'entreprise de capitaliser sur les connaissances au sein de la boite. 

On peut ainsi mettre tout le monde au même niveau et pousser la connaissance vers ceux qui sont intéressé.

Les techniques : 
* Le work sahodwing
* Les évenements , meet-up, les ateliers, hand-on
* formation
* Storytelling, Forums.

C'est toujours plus facile et à encourager de faire cela en face à face, car le contact est plus facilité.

On peut également passer par un outil(wiki, newsletter, ...).

**Le registre des leçons apprises**
il consigne toute la connaissance créée en incluant :
* une description de la situation
* une catégorisation
* un impact
* des recommandations, et actions proposées
* Eventuellement des challenges, problèmes, risques et opportunités avérés;


### Gérer la qualité

C'est de la responsable du chef de projet même si on au sein du projet on fait appel à un responsble qualité.

La gestion de la qualité est souvent appelée ‘Assurance qualité’, mais le terme de ‘Gestion’ a une étendue plus vaste.

Dans le contexte d’un projet,processus de fabrication du produit et sur les processus de **l'assurance qualité** porte sur les  management du projet tandis que produit et ses livrables **le contrôle qualité** porte sur le produit et ses livrables.


La sortie principale est le rapport Qualité.

**Diagramme Cause-effet ou Ishikawa**

Le but est de faire travailler le plus de monde possible autour d'un problème pour identifier les causes possibles à un problème qualifier.

Privilégier le support papier pour éviter que les timides ne prennenet pas la paroles.

* Les 5 ou 6 M sont ensuite placés sur un poisson 


Matières  -   Matériel -    Milieu
 


* Analyse de la "**ROOT CAUSE**" : 5 why's 
La démarche consiste à se poser la question "Pourquoi ?" au moins 5 fois de suite pour être sûr de remonter à la cause première.

* Pareto

* Audits

**Résolutions de problèmes**

Le but de la résolution de problèmes est de trouver des solutions aux problèmes ☺

Les moyens mis en œuvre peuvent être : 
* pensée critique
* Créativité
* Approche quantitative et/ou logique

Les techniques : *[la Roue de Deming](https://www.piloter.org/qualite/roue-de-deming-PDCA.htm)*


### 9.3 Acquérir des ressources
Elle se fait après l'estimation des ressources, On crée l'équipe projet et on acquiert les ressources matériels dont on a besoin.
Puis on affecte aux différentes activités. 

Elle se fait en fonction des contraintes des ressources humaines et de la politique de gouvernance de l'entreprise également;

Le pouvoir du chef de projet étant limité,il doit jouer sur son pouvoir de négociation et sur sa capacité de persuasion pour tirer son épingle du jeu et s'assurer d'avoir les meilleurs ressources.

L’acquisition des ressources doit toujours prendre conscience de certains
facteurs tels que :
* La négociation et l’influence du chef de projet pour s’assurer de l’obtention des ressources souhaitées
* Ne pas obtenir les ressources souhaitées a des impacts multiples : délais, budgets, risques, satisfaction du client, qualité, capacité, et à terme une chance de succès du projet réduite
* Si les ressources ne sont pas disponibles, des alternatives peuvent être trouvées... en restant toujours en ligne avec les contraintes légales et les exigences du projet

Ces facteurs doivent être pris en compte dans les phases de planification et l’impact de l’indisponibilité de ressources doit être documenté

**Equipe virtuelle**

PMI a arrêté de dire que ça marchait et donne plus les bonnes raisons de le faire et les ingrédients pour assurer que ça marche. Car c'est bien plus compliqué qu'en physique.


*pause*

### 9.4 Développer l'équipe

Un fois les membres de l'équipe acquise , il faut développer l'équipe maintenant et les individus.

Pour cela : 
* il faut améliorer les compétences
* Améliorer l'interaction entre les membres de l'équipe et l'environnement global de l'équipe
* afin d'améliorer la performance du projet . 

Le CP doit avoir des qualités pour former l'équipe, la motiver, la conduire et l'inspirer. 

**Technique de résolution de conflits**
* Le retrait ( withdrawing/avoiding)
* Apaisement (smoothing/accommodating)
* Compromis (compromising)
* Passage en force (Forcing)
* Résolution de problèmes (Confronting/Collaborating problem solving)

**les théories de la motivation**

**motivation**: Ce qui incite l’individu à fournir une prestation de qualité et en quantité
suffisante.

Il y'a une nuance entre : <br/>
*avoir de l'autorité, faire autorité et être autoritaire(faire preuve d'autorité)*


Facteurs de la motivation
* Interpersonnels : rapports entre individus 
* Organisationnels : travail dans l'entreprise 
* Personnels


Les Théories:
* La pyramide des besoins (A. Maslow)

En premier, l'être humain doit combler ses besoins physiologiques. Ensuite, l'être humain tentera de combler ses besoins de sécurité. Il ne peut pas combler ses besoins de sécurité avant d'avoir comblé ses besoins physiologiques. Une fois ces deux besoins comblés, viennent ensuite les besoins sociaux

* Théorie des 2 facteurs : F. HERZBERG

Pour Herzberg, la motivation ne peut pas provenir uniquement de l’élimination des facteurs d’insatisfaction.
il faut également une augmentation des facteurs de motivation.

* Théorie XY : D. Mac Gregor

Le cercle vicieux VS le cerlce vertueux ! 

L'organisation est construite sur des règles
strictes et des contrôles sévères.Les employés s'adaptent en choisissant de travailler au minimum, et en adoptant une attitude passive.
<br/>L'organisation est construite autour de
principes de confiance, de délégation et
d'autocontrôle. Les employés utilisent cette liberté supplémentaire pour mieux s'impliquer dans le travail.

Théorie Y, plus proche de théorie Marxiste.(lemodèle Google)
Théorie X , plus proche du fordisme.

*Les autres théories : cours 07 - slide 40*

**Team Building**

*L'équipe de Bruce Tuckman*

Forming -> storming -> Norming -> performing -> Adjourning

Il n’est si bonne compagnie qui ne se quitte.

### 9.5 Diriger l'équipe

C'est le processus qui consiste :
* à suivre la performance des membres de l'équipe
* à fournir des retours d'information
* à résoudre des problèmes & gérer les conflits
* et à gérer des modifications en vue d'optimiser la performance du projet

Diriger une équipe de projet requiert des compétences très variées : une combinaison de compétences de communication, de gestion de conflit, de négociation et de leadership.

### 10.2 Gérer les communications

Ne pas communiqué, c'est communiqué à la personne en face qu'elle n'est pas très importante.


Une communication efficace dépasse la seule mise à disposition des informations et implique de s'assurer que l'information a été correctement
générée, reçue et **comprise**.

Elle implique également de donner aux destinataires l'occasion de requérir des suppléments d'informations, des clarifications ou précisions.



### 11.6 Appliquer les réponses aux risques

### 12.2 Procéder aux approvisionnements

### 13.3 Manager l'engagement des parties prenantes
On essaye de réduire la resistence aux changements pour un projet.

## Surveillance et Maîtrise
12 processus 

Le schéma géneral : les données de performance en entrée
de tous les processus,

Au milieu on a un outillage , ce outillage est systématique une comparaison entre les donnée de planification et les donnée d'exécution.

En sortie on a les données de perfomance du travail. 
Et en consolidation on a les rapport d'analyse de performance du travail.

Le but est de s'assurer que tous les domaines de connaissances sont gérés comme dans le planning. 

maitriser le contenu == veiller à ce que ce qui es tlivrer est uniquement ( et pas plus ) ce


### 4.5 Surveiller et maîtriser le travail du projet

GAP analysis

**Le flux de l'information de performance**

les processus de surveillance et de maitrise vise à **collecter** les informations pour l'analyse et la comparaisaon avec les données de planificaiton ( les données prévues - ex : date prévue de livrables).

Une fois comparée la donnée devient une donnée **intégrée**

Une fois analysé, ces données sont consolidées sur un rapport que l'on résume dans le 10.2

### 4.6 Mettre en œuvre la maîtrise intégrée des modifications

Dans l'agile c'est différent car on intègre les changements à chaque fois en début de sprint.

C'est le processus qui consiste en l'examen de toutes demandes de modification, en leur étude, leur approbotion ou leur acceptation. 

Elle peut concerner les livrables, la baseline, les actifs organisationnels(souvent rares), ou le plan de management de projet.

*Tout intervenant peut **porposer** une demande de changement*
Elle est documenté dans le registre des changements et on lui donnera la suite qui convient en fonction du plan de changement intégré.

Pour étudier un changement , il faut mesurer : les impacts, les qualités, les délais, les budgets. 

Et si le changement est accpté on le reporte dans la **baseline**.


**Le comité de contrôle de changement : Le CBC ( change control board)**

C'est par lui que toute demande de changement passe. 
C'est un processus important car il vous garantie une signature après mise à jour de la baseline. Il faut donc se rassurer que les bonnes personnes ou les personnes déleguées sont en mesure de donner l'aval nécessaire.


**Le journal de modification**


### 5.5 Valider le contenu

la validation du contenu est le contrôle qualité mais par le **client**

L'outillage principal est l'**insppection** et les **technique de décision en groupe** (vote, Etc..)

### 5.6 Maitriser le contrôle

A travers ce processus, on vérifie que les modification accptées ont :
* été mise en place 
* on suivi le canevas prévu de gestion des changements

Il documente toutes les modifications et donc toutes les modifications non journalisées vont entrainer le **SCOPE CREEP**

Si un changement a été demandé on se charge l'intégrer dans le WBS et mettre à jour les livrables associés.


### 6.6 Maitriser l'échéancier

Analyse de données : 
* la méthode de la valeur acquise
* Graphique d'avancement de l'itération ( BurnDown Chart)
* Revue de performance


### 7.4 Maîtriser les coûts
*la suite au prochain épisode :)*