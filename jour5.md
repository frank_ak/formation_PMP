### 7.4 Maîtriser les coûts

Maitriser les coûts c'est un processus par lequel on va vérifier si ce qu'on dépense dans le projet est bien ce qui est prévu. 

Contrôler les coûts : 

* Influencer les facteurs qui génèrent des changement non autorisés à la référence des coûts
* S’assurer que toutes les demandes de modifications sont prises en compte dans des délais acceptables
* Gérer les changements
* S’assurer que les dépenses du projet ne dépassent pas la limite autorisée à chaque période
* Surveiller la performance des coûts pour identifier les écarts par rapport à la référence des coûts

les outils principaux : 
* analyse de la valeur acquise
* indicateur de performance pour l'achèvement ( TCPI )

#### La valeur acquise
Pratiquement, même s’il n’y a pas de directives claires à ce sujet, il sera bon de veiller à ce que les activités soient en complétude 0% ou 100% (diminution significative du temps de suivi)

*Le modèle en 0, 1 et le modèle au prorata et le 0,50,100*

Elle a comme postulat que : **tout projet est censé produire de la valeur**

*les acronymes sont en anglais*

La valeur acquise (EV earned value ): c'est la valeur crée depuis le début jusqu'à l'instant t

La valeur réelle ( AC ) : la valeur consommée depuis le début jusqu'à l'instant t

La valeur planifié ( PV ): la valeur planifié à cet instant du projet. 




> EV = PV * % complétude

> on ne compare jamais l'EV et la PV simplement si on ne connait pas le % de complétude.

Slides 27

BAC = budget ahead completed

**ANALYSE de l'écart**

> /\1 = EV - PV = SV (schedule variance)

si SV => 0 on est à l'heure ou en avance.

> /\2 = EV - AC = CV ( cost varianve )

Si CV => 0 on est dans les budget ou on est pile


**CALCUL DES INDICATEURS**

>CPI = EV / AC
> CPI : cost performance indicator

>SPI = EV/PV
>schedule performance indicator

50% de SPI veut dire que pour chaque jour de travail je perds une démi-journée.

56% de CPI signifie que sur l'ensemble dépensé 56% sont inutile en terme de valeur.

*La règle des 7* : si 7 fois d'affilé on s'éloigne de la norme dans la même tendance, il faut agir.

**Prévisions des coûts**

*1er cas de figure*

> EAC = AC + BAC - EV

On part du principe qu'un écart à eu lieu, mais qu'il est accidentel.

Coût final estimé basé sur le coût estimé pour achèvement en utilisant l’indice de performance des coûts le taux budgété

*2eme cas de figure*

> EAC = BAC / CPI aggregated

On suppose que le projet va progresser au même rythme


*3eme cas de figure*

Le budget à l'achevement sera autant influencé par CPI que par le SPI
 > EAC = AC + [(BAC – EV) / (CPI aggregated * SPI aggregated)]

 EX : slide 36

Formule de l'écart à l'achevement :
> VAC = BAC - EAC


Indice de performance pour l’achèvement du projet : 

> TCPI (BAC) = (BAC - EV) / (BAC - AC)
>
>TCPI (EAC) : (BAC - EV)/(EAC - AC)

**Concepts additionnels de la valeur acquise**

* Le reste à faire : 

> ETC = (BAC-EV)/CPI

l'ETC(reste à faire) correspond à une ré-estimation périodique du montant des travaux restant à réaliser

deux méthodes : 
    
    > On peut demander pour chaque activité le reste à faire  à chaque ressource 
    > calculer le reste à faire sur base de la performance passée, selon la formule ci-dessous.


* Le Coût estimé à l’achèvement : 
il peut alors en être déduit par la formule 

>ETC = AC + ETC aggregated

récap slides 39

### 8.3 Contrôler la qualité

On regarde à travers le contrôle qualité , le produit lui même mais aussi les processus prévues dans la planification.

Les mesures en sortie de ses processus sont des inputs pour le contrôle qualité. 

Assurance qualité VS contrôle qualité.

une entorse au processus est un défaut d'assurance qualité mais un mauvais livrables relève du contrôle qualité.

### 9.6 Contrôler les ressources

Le contrôle des ressources doit intervenir tout au long du projet

La mise à jour des affectations de ressources physiques présuppose de connaître la consommation effective de ressources et les ressources qui restent requises. 

Tout changement devra être documenté et approuvé.(change request -> modif de baseline ou MAJ -> revalidation).

### 10.3 Suivre les communications

C'est s'assurer que la communication est adéquate et bien transmise et reçue.

Un bonne communication doit atteindre sa cible et avoir de l'impact auprès de ses destinataires.

A la suite on peut refaire le plan de communication si on se rend compte de la présence de lacunes.


### 11.7 Surveiller les risques

C'est le processus qui consiste en la surveillance de la mise en œuvre des plans de réponse, en l’identification et l’analyse de nouveaux risques et en l’évaluation de l'efficacité des processus de management des risques tout au long du projet.

### 12.3 Contrôler les approvisionnements

C'est le processus qui consiste :
* A gérer les relations avec les fournisseurs
* A suivre les performances contractuelles et, le cas échéant, à effectuer les modifications et les corrections nécessaires.
* Suivi du contrat
* Suivi qualité
* Suivi administratif et financier
* Suivi de la relation client

### 13.4 Surveiller l’engagement des parties prenantes
On surveille si le niveau d'engagement reste à un bon niveau et on replanifie de nouvelles stratégies si les parties prenantes n'ont pas assez d'engagement vis à vis du projet.

Les outils :
On va chercher du feedback, on écoute, on fait du networking,

## Clôture

### 4.7 Clôturer le projet ou la phase
La liste de actions de clôture est longue et il faut bien veuiller à le faire pour ne plus intervenir sur le projet en question car il est terminé. 

* satisfaction des critères de clôture
* accords contractuels
* activités nécessaires 

L'ensemble des registres deviennent des actifs organisationnels pour le client 

En cas de mauvaise fin , on fait une réunion pour en dégager les causes et les consignés dans un rapport final. 

Les sorties : 
* rapport final
* Mise s à jour des actifs organisationnels du projet


### Rapport Final
Donne de manière sommaire tous les indicateurs projet , on redécrit le projet et les phases. 
On précise les écarts, et on les documente. On reférence ce qui a été accpeté par le client et on renseigne le pourcenteage de définition of done sur le projet. 

Il y'a également toute und documentation sur  le transitionnemnet du risque. 

### MAJ des actifs organisationnels.

Normalement à la fin de chaque projet l'organisation doit être gagnate car la base des actifs organisationnels s'enrichit. 

On gagne en maturité et on crée de la valeur.

