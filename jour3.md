Jour 3 : 01/08/2018

## Planifier le management des domaines de connaissance

il s'agit de 9 processus de 5 à 13 ( le premier processus à chaque fois) sauf 4 qui consolide l'ensemble.

Objectif : A la fin du passage dans ces 9 processus, on aura toute la partie méthodologique du plan de management projet.(qui contient la planification , puis les méthodologies).

Leur point commun : **ils ont tous comme sortie le plan de management du projet**

### 4.2 Développer le plan de management projet (WBS)
il est baseline (au fil du projet)

C'est un outil de communication, il contient :
* tout ce qu'on va faire
* a quel niveau on le fera
* combien ça va couter
* avec qui on va le faire

Le plan de mangement de projet est l'alpha et l'oméga du projet,

il contient 2 parties : 
* des plans subsidiares ( module 5)
* les 3 réferences(baseline), incluant les réponses aux risques (module 6)

À travers le Plan de management Projet on explique comment le projet va le faire(le plan de bataille), les objectifs ( contenu, coût, delai).

Sa longueur dépend fortement de la nature du projet: sa complexité, son domaine.

*baseline* : accord signé entre les responsables projets et les parties prenantes(surtout les sponsors).

Il est mis à jour pour refléter les changements survenus en cours de projet.

**Rôle du plan de management de projet**
Selon PMI, :
* guider l'exécution du projet ( on l'a signé , on le suit)
* faciliter la communication avec les parties prenanntes
* fixer les revues de projet principales
* fournir un referentiel pour mesutrer l'avanement et la maitrise du projet


cf tableau  in-out du plan de mangement projet

Focus group : atélier centré sur une problèmatique précise. 
souvent à la suite d'un branstorming et du choix d'une idée;

**Réunions**
* spécifiques aux réunions de planning ( focus group)
* Kick-off clôture habituellement la fin du planning et marque le début de l'éxecution
    * communiquer les objectifs projet
    * gagner l'engagement des ressources et des parties prenantes
    * décrire les rôles et responsabilité de chacun

Dans un petit projet, le kick-off est organisé rapidement après l'initiation, pour inclure l'équipe dans les processus de planning. 

**Contenut du plan de management du projet**
slide5 p10



### 5.1 planifier les management du contenu 

l'input majeur ici c'est la charte du projet.

Sur la base de la charte, des facteurs environnementaux de l'entreprise, des actifs organisationnels, et le plan de management du projet; on crée en sortie : le plan de managemnt du contenu et le plan de mangement des exigences.

**Plan de managemnt du contenu**

C'est une partie du plan de managment projet
il contient : 
* comment le contenu va être défini, développén suivi, contrôlé, maîtrisé.
* scope stattement
* WBS et sa maintenance
* processus définissant l'acceptation formelle des livrables
* du processus définissant la méthode de modification du contenu

**Plan de managemnt du des exigences**

Décrit comment les exigences des parties prenantes seront recueillies, analysées, documentées et gérées.

le plan peut également définir comment les exigences seront mesurées.

Définir la structure de traçabilité et les priorités.

Méthode **[MOSCOW](https://fr.wikipedia.org/wiki/M%C3%A9thode_MoSCoW)**

### 6.1 planifier les management des délais
il est fortement récupérable d'un projet à l'autre.

Comment on va prendre les pertubations de temps sur notre projet et comment on intervient pour corriger.

Les activités nécessaire pour suivre et contrôler l'échéancier.
On y explique comment on fait les échéancier, le niveau de précision sur les tâches, la durée de sprint (en agile par exple), quel est l'unité de mesure de l'effort.

La mesure du temps en agile est différente :
https://blog.myagilepartner.fr/index.php/2017/11/02/parle-t-on-de-complexite-ou-de-point-d-effort/

Définir le seuil de retard non rattrapable sans ressource nécessaire, ou delà duquel on doit prevenir les parties prenantes.

> Schedule performance indicator 

### 7.1 planifier les management des Coûts
Il explique comment on gère , contrôle et gère les pertubatiosn qui peuvent nuirent à notre budget.

Le niveau de précision est important. La définition de l'unité financière dans l'organisation.

Le seuil de tolérance en terme d'écart budgétaire.

### 8.1 planifier les management de la Qualité

3 processus de qualité ! 

Une fois encore on ne demande pas au chef de projet d'être le responsable qualité mais de faire l'intégration des retours qualitatifs dans le plan managemnt projet. 

[Six sigma](https://qualite.ooreka.fr/comprendre/six-sigma)
 et [Lean](https://www.planzone.fr/blog/quest-ce-que-la-methodologie-lean-six-sigma)

 il y'a énormement de technique de planification de la qualité, seuls les plus fréquemments utilisées sont décrites dans PMBok 6

**in-out**

en sortie on a des KPIs, le plan de managemnt de la qualité.

**Analyse des coûts**

Le coût optimal de la qualité reflète l’équilibre adéquat entre l’investissement dans la prévention et l’évaluation pour éviter les coûts de non-conformité. Investir plus dans la prévention lorsque l’optimum est atteint n’est ni rentable, ni profitable

**Diagramme de Flux**

Aussi appelé **Process MAP**, est le diagramme qui montre les différentes chemins possibles en fonction des use case, et qui présente les activités , les points de décision et les boucles de branchements et les flux parallèles.

[SWIM LANE](https://en.wikipedia.org/wiki/Swim_lane) est une bonne forme de diagramme de flux car il implique les responsables des actions en plus des actions.

**Diagramme en SIPOC**

Dans ce diagramme on peut identifier les bottleNeck et les points de satisfaction client important.

### 9.1 planifier les management des ressources

La plannification doit permettre d'identifier les besoins en charge et de prendre de l'avance sur les demandes en fonction de la gouvernance.

in-out

La charte et la réference du contenu sont les inputs.

RACI, annonce de poste ou fiche de poste.

RBS : ressource breakdown structure.
Le but est de s'assurer que chaque lot est lié à une et une seule ressource ( la responsabiilité ) et que tous les lots ou livrables sont attribués aux rssources projet.

**Organigrammes**

* Format **Top Down** traditionnel utilisé pour monter les positions hiérarchiques.
    * le WBS est de ce type
    * OBS ( Organizational Breakdown Structure)

**Matrices dedescription de postes**

RACI

* pour chaqeu activité il faut au moins 1 R ( celui qui fait) et pas plus d'un A (accountable).


En somme : 

Le plan de managent de ressources contient :
* L’identification des ressources
* La méthode d’acquisition des ressources
* Rôles et responsabilités, autorités et compétences 
* Organigrammes du projet
* La gestion des ressources de l’équipe du projet
* Besoins en formation et développement de l’équipe 
* Le contrôle des ressources
    * Méthodes pour s’assurer que les ressources physiques sont bien disponibles.
    * On y inclut des informations   sur la gestion des inventaires, des équipements et des fournitures. 
* Plan de **récompenses et reconnaissance**


#### LA CAHRTE DE L'ÉQUIPE
C'est une sorte de code de conduite pour l'équipe, ce qui permet de définir la valeur de chaque chose pour chacun.

### 10.1 planifier les management de communication
Le processus est itératif tout au long du projet.
Il consiste à :
* déterminer les besoins en information des parties prenantes du projet
* définir une approche pour les communications

La plannification de la communication donne une approche documentée pour engager les parties prenantes dans le projet.

Le nombre max de canaux de comm : 

nbre = n(n-1)/2 

N étant le nombres de personnes impliqués dans le projet.

Les modèles de communication : 
* Modèle simple
    <br/>encoder -> transmettre -> décoder
* Communication interactive
    <br/>second étape  ajoutés : feedback/accusé de reception/

Méthodes de communications : **PUSH vs PULL**

Artéfacts et méthodes de communication : il s'agit des produits humains.

* tableaux de notification
* Newsletters, e-zins
* Lettres
* Emails,...

en Sortie on a donc : 
* Le plan de managemnt des communications
    * la planification de communication projet, les structures, la gestion des version et des configurations de cette communication.
    * Le procssus d'escalade en cas de conflit
    * la fréquence de distribution, les méthodes en fonction des destinataires, le budget, le dictionnaire du projet, le schéma des flux d'informations.

PMIS : le project management information system.

### 11.1 planifier les management des risques

On identifie **COMMENT** on va conduire les activités de risque. Cela doit rester rentable et pour cela il faut faire intervenir le bon sens.

Cela se fait en rapport avec la tolérance au risque de nos parties prenantes et de leur appetit au risque. 

La catégorisation des risques : 

* Le RBS (Risk breakdown structure):
* regrouper les risques individuels du projet
* utiliser le modèle de l'entreprise s'il existe.


RBS dans le glossaire veut dire deux choses Risk et Ressource breakdown structure l'un est un outil et l'autre un livrable ou une sortie qui fait partie du plan de management de projet.

Matrice de probabilité et d'impact
* les règles de priorité peuvent être spécifié à l'avance

Format de reporting et le format de traçage.

### 11.1 planifier les management des approvisionnements

C'est le processus qui consiste : 
* documenter le décisions d'approvisionnement du projet
* a spécifier les méthodes et approches qui seront utilisées
* identifier les vendeurs potentiels

L'approvisionnement se résume en 3 étapes : 
* TOR ( term of reference), SOW (statement of work)
* Préparer une estimation à haut niveau des coûts

Les méthodes de sélection : 
* Le moindre coût
* qualification seule
* Quality-based- Meilleure note technique
* Quality and Cost based /!\ attention à laisser la qualité en priorité
* Sole source
* Budget fixed

En sortie on a : 
* comment l'approvisionnement est coordoné aux autres aspects du projet
* Jalons pour les activités principales d'approvisionnement


Document de soumission

RFI -> RFP | RFQ -> 


### 13.1 planifier l'**engagement** des parties prenantes

C'est le processus qui consiste à développer une stratégie d'engagement des parties prenantes, basée sur l'analyse de leurs besoins, intérêts et impacts.

L'objectif est d'élaborer un plan d'action pour interagir avec les parties prenantes dans le maielleur intérêt du projet

Au final on en sort avec le plan d'**engagement** des parties prenantes qui doit garder une certaines forme de confidentialité.

Matrice d'évaluation de l'engagment des parties prenantes

* Unaware
* Resistant
* Neutral
* Supportive
* Leading

Pour chaque partie prenante on va mettre une de ces lettres pour envisager une politique ou une méthodologie de communication ou d'engagement avec ça.

En sortie : 
* le plan d'engagement des parties prenantes




## Définir les référentiels (baseline)

Contenu du module 6 :
* définir le contenu
* définir et organiser les activités
* estimer
* Elaborer l'echeancier
* determinerle budget
* planifier les risques

Objectifs de la séance : 

Cette séance a pour objectif de décrire les processus nécessaires à :
* définir et formaliser le contenu du projet
* la définition du contenu est orientée "Livrables"

Le résultat de ce passage dans 3 processus de planification est :
* un ensemble de documents qui constitue un document de réference du contenu projet
* un esemble de doc important pour le contenu du projet
* Suivre et maintenir

[La règles des 100%](http://www.workbreakdownstructure.fr/regle-100-pourcent.php)

### 5.3 Définir le contenu

il consiste à élaborer le :
* le scope statement (ce qui tombe un peu en désuètude)
* décrire le produit et le projet pour permettre la constitution ultérieure du WBS

le Scope statement est sur le même format que la charte mais en version bien plus détaillé. 

On décrit le produit, les livrables, ce que le projet exclu explicitement pour éviter les négociations dûes au chage request

### 5.4 Créer la strutuce de découpage du projet (SDP/WBS)

* c'est le processus qui consiste à subdiviser les livrables et le travail du projet en composants plus petits et plus faciles à maîtriser

* Les composants sont disposés dans un diagramme hiérarchique, le WBS (Work Breakdown Structure) ou SDP (Structure de Découpage du Projet)

* Le dictionnaire du WBS
   

**La décomposition**

On fait du top down et du bottom up pour affiner, identifier les librables et on attribue des identifiants au WBS.

Elle peut se faire sur la base du produit ou de phases
* décomposition produit
* décomposition par phases

**La référence du contenu**

version approuvée de l'énoncé du contenu, du WBS et de son dictionnaire.

##  Définir les activités et les organiser

### 6.2 Définir les activités

On découpe nos activités en activités planifiables tangibles, on identifie bien les livrables et les estimations (de temps) associées.

Le niveau de l'activité doit être assignable pour être passer en exécution.

### 6.3 Organiser les activités en séquence

Toutes les activités doivent avoir une précédente et une suivante sauf la première et la dernière : c'est le séquencement.

Attention à séquencer les activités mais pas les livrables (qui englobent les activités). C'est pas une obligation mais une bonne pratique. 

Ex: les activités d'un autre livrable peuvent être séquencées vis à vis des activités d'un autre livrable. mais cela n'induit pas de séquenceemnt entre ces livrables.

représentation des dépendances

Ajout des retards des délais.

Calcul des chemins critiques. ( PERT ou GANT)

**Méthodes des antécécédants
* FS (finish - start)
* FF
* SS
* SF

**Détermination des dépendances**
* Obligatoire : inhérente à la nature du travail
* Facultative (discrétionnaire): la logique préférable, mais la dépendance peut être levée si une autre route est choisie. 


* Externe (habutuellement ce genre de dépendance doit être placée comme un **risque**)
* interne


**Avances et Retards ( Leads and Lags )
* inclusion d'une durée au sein d'une dépendance

>Le lead : est une parallélisation 
><br/>Le lag est un temps de séchage

**Diagramme en réseau de l'échéancier**


## Estimer

* L'estimation est toujours fausse
* L'estimation est mieux que zéro

Il faut chercher la point du cône d'incertitude

* Loi de hofdstader
* Parkinsons's squeeze

### 6.4 Estimer la durée des activités

* La loi des retours en diminution
* Ajouter des gens peut être contre productif
* Avancées technologiques
* Motivation de l'équipe


**Durée VS charge**

40 jh == 20 jours pour 2 ressources
ET 30 jours en comptant une charge nulle de base et les jours ouvrées de travail. 

### 7.2 Estimer les coûts

### 9.2 Estimer les ressources nécessaires aux activités

C'est le processus qui consiste à définir le profil des personnes qui feront partie de l’équipe de projet, et les quantités de matériel nécessaire, des équipements et fournitures.

On parle évidemnt de coût et de délai car on fait habituellement les trois dans la foulé.

L'estimation des ressources est égalemnt assez indissociable de l'approvisionnement en ressources dans le projet ou des coût de formation ou d'intervention d'experts (ce qui entraine des coûts ou un temps supplémentaire).

activité -> nom
sur les nom -> cout et durée.


**RBS : organigramme des ressources**

Toutes les ressources humaines ou matérielle.

## Élaborer l'échéancier

